{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Category
open import Functor
open import Utils
open import Utils.Path

open Category.Category
open Category.PreCategory
open Category.RawCategory
open Functor.Functor

module Categories.Product where

  record ProductCat
    {lA lA' lB lB'}
    (A : Category lA lA')
    (B : Category lB lB')
    : Type (lsuc ((lA ⊔ lB) ⊔ (lA' ⊔ lB')))
    where
      field
        cat : Category (lA ⊔ lB) (lA' ⊔ lB')
        π₁ : Functor cat A
        π₂ : Functor cat B

  open ProductCat

  PROD : ∀ {lA lA' lB lB'}
    → (A : Category lA lA')
    → (B : Category lB lB')
    →  ProductCat A B
  PROD {lA} {lA'} {lB} {lB'} A B = result where

    rawCat : RawCategory (lA ⊔ lB) (lA' ⊔ lB')
    rawCat .Object = A .Object × B .Object
    rawCat .Arrow (xA , xB) (yA , yB) = A .Arrow xA yA × B .Arrow xB yB
    rawCat .id {a , b} = A .id , B .id
    rawCat .seq (ga , gb) (fa , fb) = ga ∙⟨ A ⟩ fa , gb ∙⟨ B ⟩ fb
    rawCat .seq-id-l (fa , fb) i = A .seq-id-l fa i , B .seq-id-l fb i
    rawCat .seq-id-r (fa , fb) i = A .seq-id-r fa i , B .seq-id-r fb i
    rawCat .seq-assoc (fa , fb) (ga , gb) (ha , hb) i =
      A .seq-assoc fa ga ha i , B .seq-assoc fb gb hb i

    preCat : PreCategory (lA ⊔ lB) (lA' ⊔ lB')
    preCat .isRawCategory = rawCat
    preCat .isSet-Arrow = isSet× (A .isSet-Arrow) (B .isSet-Arrow)

    category : Category (lA ⊔ lB) (lA' ⊔ lB')
    category .isPreCategory = preCat
    category .≈→≡ ((fa , fb) , (fa⁻ , fb⁻) , sec , ret) i =
      A .≈→≡ (fa , fa⁻ , ap fst sec , ap fst ret) i ,
      B .≈→≡ (fb , fb⁻ , ap snd sec , ap snd ret) i

    result : ProductCat A B
    result .cat = category

    result .π₁ .map∘ = fst
    result .π₁ .map→ = fst
    result .π₁ .map-id _ = refl
    result .π₁ .map-dist _ _ = refl

    result .π₂ .map∘ = snd
    result .π₂ .map→ = snd
    result .π₂ .map-id _ = refl
    result .π₂ .map-dist _ _ = refl
