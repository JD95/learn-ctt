{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Data.Sum
open import Agda.Primitive
open import Cubical.Foundations.HLevels
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Agda.Builtin.Cubical.Path
open import Cubical.Foundations.Univalence

open import Category
open import Functor
open import Utils
open import Utils.HLevels
open import Utils.Path

open Category.Category
open Functor.Functor

module Categories.Set where
  raw-category : ∀ {l} → RawCategory (lsuc l) l
  raw-category {l} .RawCategory.Object = Σ (Type l) isSet
  raw-category .RawCategory.Arrow (a , _) (b , _) = a → b
  raw-category .RawCategory.id x = x
  raw-category .RawCategory.seq g f = λ x → g (f x)
  raw-category .RawCategory.seq-assoc f g h = refl
  raw-category .RawCategory.seq-id-l f = refl
  raw-category .RawCategory.seq-id-r f = refl

  pre-category : ∀ {l} → PreCategory (lsuc l) l
  pre-category .PreCategory.isRawCategory = raw-category
  pre-category .PreCategory.isSet-Arrow {_} {_ , y/set} = isSetΠ λ _ → y/set

  category : ∀ {l} → Category (lsuc l) l
  category .Category.isPreCategory = pre-category
  category .Category.≈→≡ {a , a/set} {b , b/set} (f , g , l-id , r-id) i = obj≡ i , set≡ i where

    obj≡ : a ≡ b
    obj≡ = isoToPath (iso f g (funExt⁻ l-id) (funExt⁻ r-id))

    set≡ : PathP (λ i → isSet (obj≡ i)) a/set b/set
    set≡ = isSet→PathP (toPath obj≡) a/set b/set

  SET : ∀ {l} → Category (lsuc l) l
  SET = category
