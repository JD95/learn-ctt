{-# OPTIONS --cubical #-}

module modalities where

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Agda.Builtin.Sigma

record IsOneIdent {M : Set} (_⨂_ : M → M → M) (one : M) : Set where
  field
    ident-r : (a : M) → a ⨂ one ≡ a
    ident-l : (a : M) → one ⨂ a ≡ a

record IsZeroIdent {M : Set} (_⨂_ : M → M → M) (zero : M) : Set where
  field
    ident-r : (a : M) → a ⨂ zero ≡ zero
    ident-l : (a : M) → zero ⨂ a ≡ zero

record IsAssoc {M : Set} (_⨂_ : M → M → M) : Set where
  field
    assoc : (a b c : M) → (a ⨂ b) ⨂ c ≡ a ⨂ (b ⨂ c)

record IsComm {M : Set} (_⨂_ : M → M → M) : Set where
  field
    comm : (a b : M) → a ⨂ b ≡ b ⨂ a

record IsIdem {M : Set} (_⨂_ : M → M → M) : Set where
  field
    idem : (a : M) → a ⨂ a ≡ a

record IsDist {M : Set} (_⨂_ : M → M → M) (_⨁_ : M → M → M): Set where
  field

    dist-l : (a b c : M) → a ⨂ (b ⨁ c) ≡ (a ⨂ b) ⨁ (a ⨁ c)
    dist-r : (a b c : M) → (b ⨁ c) ⨂ a ≡ (a ⨂ b) ⨁ (a ⨂ c)

record IsRingModality
  (M : Set)
  (_+_ _*_ _⋀_ : M → M → M)
  (zero one : M)

  : Set where

  field

    +-zero-ident : IsOneIdent _+_ zero
    +-comm : IsComm _+_
    +-assoc : IsAssoc _+_

    *-zero-ident : IsZeroIdent _*_ zero
    *-one-ident : IsOneIdent _*_ one
    *-assoc : IsAssoc _+_

    ⋀-comm : IsComm _⋀_
    ⋀-assoc : IsAssoc _⋀_
    ⋀-idem : IsIdem _⋀_

    *-dist-over-+ : IsDist _*_ _+_
    *-dist-over-⋀ : IsDist _*_ _⋀_
    +-dist-over-⋀ : IsDist _+_ _⋀_

data Linear : Set where
  l0 : Linear
  l1 : Linear
  lw : Linear

module Linear-Modality where

  _+_ : Linear → Linear → Linear
  l0 + a  = a
  l1 + l0 = l1
  l1 + l1 = l1
  l1 + lw = lw
  lw + a = lw

  _*_ : Linear → Linear → Linear
  l0 *  _ = l0
  l1 * l0 = l0
  l1 * l1 = l1
  l1 * lw = lw
  lw * l0 = l0
  lw * l1 = lw
  lw * lw = lw

  _⋀_ : Linear → Linear → Linear
  l0 ⋀  a = a
  l1 ⋀ l0 = l1
  l1 ⋀ l1 = l1
  l1 ⋀ lw = lw
  lw ⋀  _ = lw

  zero : Linear
  zero = l0

  one : Linear
  one = l1

  +-zero-ident : IsOneIdent _+_ zero
  +-zero-ident = record {ident-r = ident-r; ident-l = ident-l} where
   ident-r : (a : Linear) → a + zero ≡ a
   ident-r l0 i = l0
   ident-r l1 i = l1
   ident-r lw i = lw

   ident-l : (a : Linear) → zero + a ≡ a
   ident-l l0 i = l0
   ident-l l1 i = l1
   ident-l lw i = lw

  +-comm : IsComm _+_
  +-comm = record {comm = comm}
    where
      comm : (a b : Linear) → a + b ≡ b + a
      comm l0 b = +-zero-ident.ident-r b
      comm l1 b i = {!!}
      comm lw b i = {!!}

  -- +-assoc : IsAssoc _+_

  -- *-zero-ident : IsZeroIdent _*_ zero
  -- *-one-ident : IsOneIdent _*_ one
  -- *-assoc : IsAssoc _+_

  -- ⋀-comm : IsComm _⋀_
  -- ⋀-assoc : IsAssoc _⋀_
  -- ⋀-idem : IsIdem _⋀_

  -- *-dist-over-+ : IsDist _*_ _+_
  -- *-dist-over-⋀ : IsDist _*_ _⋀_
  -- +-dist-over-⋀ : IsDist _+_ _⋀_
