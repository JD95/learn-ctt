{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Logic where

open import Agda.Builtin.Unit
open import Data.Sum
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism

open import Utils.Void
open import Utils.Sum
open import Utils

¬_ : (A : Type) → Type
¬_ A = A → ⊥

non-contradiction : {P : Type} → P × (¬ P) → ⊥
non-contradiction (p , ¬p) = absurd (¬p p)

demorgan-⊎
  : {A B : Type}
  → ¬ (A ⊎ B) ≡ (¬ A) × (¬ B)
demorgan-⊎ {A = A} {B = B} = isoToPath (iso f g sec rec) where

  f : ¬ (A ⊎ B) → (¬ A) × (¬ B)
  f x = (λ a → x (inj₁ a)) , (λ b → x (inj₂ b))

  g : (¬ A) × (¬ B) → ¬ (A ⊎ B)
  g (a , b) (inj₁ x) = absurd (a x)
  g (a , b) (inj₂ y) = absurd (b y)

  sec : (x : (¬ A) × (¬ B)) → f (g x) ≡ x
  sec (a , b) i
    = (λ x → isProp⊥ (absurd (a x)) (a x) i)
    , (λ x → isProp⊥ (absurd (b x)) (b x) i)

  rec : (p : ¬ (A ⊎ B)) → g (f p) ≡ p
  rec p i x = isProp⊥ (g (f p) x) (p x) i

disjunct-l : {A B : Type} → (A ⊎ B) → ¬ A → B
disjunct-l (inj₁ x) ¬a = absurd (¬a x)
disjunct-l (inj₂ y)  _ = y

disjunct-r : {A B : Type} → (A ⊎ B) → ¬ B → A
disjunct-r (inj₁ x)  _ = x
disjunct-r (inj₂ y) ¬b = absurd (¬b y)

conjunct-l : {A B : Type} → A → ¬ B → ¬ (A × B)
conjunct-l a ¬b (_ , b) = ¬b b

neg-conjunct-l : {A B : Type} → ¬ (A × B) → B → ¬ A
neg-conjunct-l p b a = p (a , b)

conjunct-r : {A B : Type} → ¬ A → B → ¬ (A × B)
conjunct-r ¬a b (a , _) = ¬a a

neg-conjunct-r : {A B : Type} → ¬ (A × B) → A → ¬ B
neg-conjunct-r p a b = p (a , b)

demorgan-×₁ : {A B : Type} → ¬ A ⊎ ¬ B → ¬ (A × B)
demorgan-×₁ (inj₁ p) (a , _) = p a
demorgan-×₁ (inj₂ p) (_ , b) = p b

demorgan-×₂ : {A B : Type} → ¬ (A × B) → ¬ A ⊎ ¬ B
demorgan-×₂ p = {!!}
