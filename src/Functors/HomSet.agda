{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Utils
open import Category
open import Functor
open import Categories.Set

open Functor.Functor
open Category.Category
open Category.RawCategory
open Category.PreCategory

module Functors.HomSet where

  HOMSET : ∀ {lo la} (C : Category lo la) → (a : C .Object) → Functor C SET
  HOMSET C a .map∘ b = C .Arrow a b , C .isSet-Arrow
  HOMSET C a .map→ g f = g ∙⟨ C ⟩ f
  HOMSET C a .map-id obj i f = C .seq-id-l f i
  HOMSET C a .map-dist f g i h = C .seq-assoc h f g (~ i)

  HOMSET⁻ : ∀ {lo la} (C : Category lo la) → (b : C .Object) → Contra C SET
  HOMSET⁻ C b .map∘ a = C .Arrow a b , C .isSet-Arrow
  HOMSET⁻ C b .map→ g f = g ⟨ C ⟩∙ f
  HOMSET⁻ C b .map-id obj i f = C .seq-id-r f i
  HOMSET⁻ C b .map-dist f g i h = op C .seq-assoc h f g (~ i)
