{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Utils
open import Utils.Path
open import Category
open import Functor
open import Categories.Product

open Category.Category
open Category.PreCategory
open Category.RawCategory
open Functor.Functor
open ProductCat

module Functors.Product where

  _×F_
    : ∀ {a a' b b' c c' d d'}
        {A : Category a a'}
        {B : Category b b'}
        {C : Category c c'}
        {D : Category d d'}
    → (F : Functor A C)
    → (G : Functor B D)
    → Functor (PROD A B .cat) (PROD C D .cat)
  _×F_ F G .map∘ (a , b) = F .map∘ a , G .map∘ b
  _×F_ F G .map→ (fa , fb) = F .map→ fa , G .map→ fb
  _×F_ F G .map-id (a , b) i = F .map-id a i , G .map-id b i
  _×F_ F G .map-dist (fa , fb) (ga , gb) i = F .map-dist fa ga i , G .map-dist fb gb i

  BiFunctor
    : ∀ {a a' b b' c c'}
    → (A : Category a a')
    → (B : Category b b')
    → (C : Category c c')
    → Type (lsuc ((a ⊔ a') ⊔ (b ⊔ b') ⊔ (c ⊔ c')))
  BiFunctor A B C = Functor (PROD A B .cat) C
