{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Bool.Algebra where

open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.Isomorphism

open import Utils.Void
open import Utils

data Bool : Type where
  true : Bool
  false : Bool

true≠false : true ≡ false → ⊥
true≠false p = subst P p tt where
  P : Bool → Type
  P false = ⊥
  P true = ⊤

not : Bool → Bool
not false = true
not true = false

not-inv : {p : Bool} → not (not p) ≡ p
not-inv {true} = refl
not-inv {false} = refl

not→false : {p : Bool} → (not p ≡ true) → p ≡ false
not→false {p} fact =
  p ≡⟨ sym not-inv  ⟩
  not (not p) ≡⟨ (λ i → not (fact i)) ⟩
  false ∎

false→not : {p : Bool} → p ≡ false → (not p ≡ true)
false→not {p} fact i = not (fact i)

_and_ : Bool → Bool → Bool
_and_ false _ = false
_and_ _ false = false
_and_ true true = true

_or_ : Bool → Bool → Bool
_or_ true _ = true
_or_ _ true = true
_or_ false false = false

_xor_ : Bool → Bool → Bool
true xor true = false
true xor false = true
false xor true = true
false xor false = false

infixl 5 _and_
infixl 5 _or_
infixl 5 _xor_

and-absorb : {p : Bool} → (p and true) ≡ p
and-absorb {true} = refl
and-absorb {false} = refl

and-cancel : {p : Bool} → (p and false) ≡ false
and-cancel {true} = refl
and-cancel {false} = refl

and-inv : {p : Bool} → (p and p) ≡ p
and-inv {true} = refl
and-inv {false} = refl

expand-xor : {p q : Bool} → (p xor q) ≡ (p and not q) or (not p and q)
expand-xor {true} {true} = refl
expand-xor {true} {false} = refl
expand-xor {false} {true} = refl
expand-xor {false} {false} = refl

xor-comm : {a b c : Bool} → (a xor (b xor c)) ≡ ((a xor b) xor c)
xor-comm {true} {true} {true} = refl
xor-comm {true} {true} {false} = refl
xor-comm {true} {false} {true} = refl
xor-comm {true} {false} {false} = refl
xor-comm {false} {true} {true} = refl
xor-comm {false} {true} {false} = refl
xor-comm {false} {false} {true} = refl
xor-comm {false} {false} {false} = refl

demorgan-1 : {p q : Bool} → (not (p or q)) ≡ (not p and not q)
demorgan-1 {true} {true} = refl
demorgan-1 {true} {false} = refl
demorgan-1 {false} {true} = refl
demorgan-1 {false} {false} = refl

demorgan-2 : {p q : Bool} → (not (p and q)) ≡ (not p or not q)
demorgan-2 {true} {true} = refl
demorgan-2 {true} {false} = refl
demorgan-2 {false} {true} = refl
demorgan-2 {false} {false} = refl

xor≡false : {p q : Bool} → (fact : (p xor q) ≡ false) → p ≡ q
xor≡false {true} {true} fact = refl
xor≡false {true} {false} fact = absurd (true≠false fact)
xor≡false {false} {true} fact = absurd (true≠false fact)
xor≡false {false} {false} fact = refl

xor≡false⁻ : {p q : Bool} → (fact : p ≡ q) → ((p xor q) ≡ false)
xor≡false⁻ {true} {true} fact = refl
xor≡false⁻ {true} {false} fact = absurd (true≠false fact)
xor≡false⁻ {false} {true} fact = absurd (true≠false (sym fact))
xor≡false⁻ {false} {false} fact = refl

xor≡true : {p q : Bool} → (fact : (p xor q) ≡ true) → p ≡ q → ⊥
xor≡true {true} {true}   fact p≡q = absurd (true≠false (sym fact))
xor≡true {true} {false}  fact p≡q = absurd (true≠false (p≡q))
xor≡true {false} {true}  fact p≡q = absurd (true≠false (sym p≡q))
xor≡true {false} {false} fact p≡q = absurd (true≠false (sym fact))

and≡true : {p q : Bool} (fact : p and q ≡ true) → (p ≡ true) × (q ≡ true)
and≡true {true} {true} fact = refl , refl
and≡true {true} {false} fact = absurd (true≠false (sym fact))
and≡true {false} {true} fact = absurd (true≠false (sym fact))
and≡true {false} {false} fact = absurd (true≠false (sym fact))

or≡false : {p q : Bool} (fact : p or q ≡ false) → (p ≡ false) × (q ≡ false)
or≡false {true} {true} fact = absurd (true≠false fact)
or≡false {true} {false} fact = absurd (true≠false fact)
or≡false {false} {true} fact = absurd (true≠false fact)
or≡false {false} {false} fact = refl , refl

proof
  : {a≡b b≡c c≡a : Bool}
  → (notEquil : (a≡b and b≡c) ≡ false)
  → (notIso : (a≡b and not c≡a) or
              (c≡a and not b≡c) or
              (b≡c and not a≡b) ≡ false)
  → (not a≡b and not b≡c and not c≡a) ≡ true
proof {true} {true} {true} notEquil notIso = absurd (true≠false notEquil)
proof {true} {true} {false} notEquil notIso = absurd (true≠false notIso)
proof {true} {false} {true} notEquil notIso = absurd (true≠false  notIso)
proof {true} {false} {false} notEquil notIso = absurd (true≠false notIso)
proof {false} {true} {true} notEquil notIso = absurd (true≠false notIso)
proof {false} {true} {false} notEquil notIso = absurd (true≠false notIso)
proof {false} {false} {true} notEquil notIso = absurd (true≠false notIso)
proof {false} {false} {false} notEquil notIso = refl
