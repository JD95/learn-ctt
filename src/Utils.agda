{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Builtin.Cubical.Path
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Agda.Primitive
open import Cubical.Foundations.Prelude

module Utils where

  open import Utils.Path
  open import Utils.HLevels
  open import Utils.Void
  open import Utils.Sum

  _×_ : ∀ {l k} (A : Type l) → (B : Type k) → Type (l ⊔ k)
  a × b = Σ a (λ _ → b)

  isProp⊤ : isProp ⊤
  isProp⊤ = λ x y i → tt
