{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Lattice where

open import Agda.Primitive
open import Agda.Builtin.Unit
open import Data.Sum
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism

open import Utils.Void
open import Utils.Sum
open import Utils

record PartialOrder {l} (A : Type l) : Type (lsuc l) where
  field
    _≤_ : A → A → Type l
    isProp≤ : (a b : A) → isProp (a ≤ b)
    reflexive : (a : A) → a ≤ a
    trans : (a b c : A) → a ≤ b → b ≤ c → a ≤ c
    anti-sym : (a b : A) → a ≤ b → b ≤ a → a ≡ b

record SemiLattice {l} (A : Type l) : Type (lsuc l) where
  field
    isPartialOrder : PartialOrder A

  open PartialOrder isPartialOrder public

  field
    op : A → A → A
    terminal : A
    bound : (x : A) → x ≤ terminal
    bound-unique : (x : A) → ((s : A) → s ≤ x) → x ≡ terminal

record Lattice {l} (A : Type l) : Type (lsuc l) where
  field
    join : SemiLattice A
    meet : SemiLattice A
