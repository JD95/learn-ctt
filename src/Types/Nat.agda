{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Types.Nat where

open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism

open import Utils
open import Utils.Void
open import Lattice

data Nat : Type where
  Z : Nat
  S : Nat → Nat

lte : Nat → Nat → Type
lte Z _ = ⊤
lte (S _) Z = ⊥
lte (S n) (S m) = lte n m

gte : Nat → Nat → Type
gte Z Z = ⊤
gte Z (S _) = ⊥
gte (S _) Z = ⊤
gte (S n) (S m) = gte n m

isProp-gte : (a b : Nat) → isProp (gte a b)
isProp-gte Z Z = isProp⊤
isProp-gte Z (S b) = isProp⊥
isProp-gte (S a) Z = isProp⊤
isProp-gte (S a) (S b) = isProp-gte a b

gte-reflexive : (a : Nat) → gte a a
gte-reflexive Z = tt
gte-reflexive (S a) = gte-reflexive a

gte-trans : (a b c : Nat) → gte a b → gte b c → gte a c
gte-trans Z Z Z _ _ = tt
gte-trans (S a) Z Z _ _ = tt
gte-trans (S a) (S b) Z _ _ = tt
gte-trans (S a) (S b) (S c) p q = gte-trans a b c p q

gte-anti-sym : (a b : Nat) → gte a b → gte b a → a ≡ b
gte-anti-sym Z Z p q = refl
gte-anti-sym (S a) (S b) p q i = S (gte-anti-sym a b p q i)

module _ where
  open PartialOrder

  partial-order : PartialOrder Nat
  partial-order ._≤_ n m = lte n m
  partial-order .isProp≤ = f where
    f : (n m : Nat) → isProp (lte n m)
    f Z Z = isProp⊤
    f Z (S m) = isProp⊤
    f (S n) Z = isProp⊥
    f (S n) (S m) = f n m
  partial-order .reflexive = f where
    f : (a : Nat) → lte a a
    f Z = tt
    f (S n) = f n
  partial-order .trans = f where
    f : (a b c : Nat) → lte a b → lte b c → lte a c
    f Z Z Z _ _ = tt
    f Z Z (S c) _ _ = tt
    f Z (S b) (S c) _ _ = tt
    f (S a) (S b) (S c) x y = f a b c x y
  partial-order .anti-sym = f where
    f : (a b : Nat) → lte a b → lte b a → a ≡ b
    f Z Z x y = refl
    f (S a) (S b) x y i = S (f a b x y i)

open PartialOrder partial-order

suc-monotonic : (a b : Nat) → a ≤ b → S a ≤ S b
suc-monotonic Z Z p = tt
suc-monotonic Z (S b) p = tt
suc-monotonic (S a) (S b) p = suc-monotonic a b p

pred≤suc : (a : Nat) → a ≤ S a
pred≤suc Z = tt
pred≤suc (S a) = suc-monotonic a (S a) (pred≤suc a)

inductive-≤ : (n a : Nat) → S a ≤ n → a ≤ n
inductive-≤ _ Z _ = tt
inductive-≤ n (S a) p = trans (S a) (S (S a)) n (pred≤suc (S a)) p
