{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Data.Sum
open import Agda.Primitive
open import Cubical.Foundations.HLevels
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Agda.Builtin.Unit
open import Agda.Builtin.Cubical.Path

open import Utils
open import Functor
open import Categories.Set using (SET)

open Functor.Functor

module Types.Maybe where

  data Maybe {l : Level} (A : Type l) : Type l where
    Nothing : Maybe A
    Just : A → Maybe A

  generic : ∀{l} {A : Type l} → Maybe A ≡ (⊤ ⊎ A)
  generic {A = A} = isoToPath (iso to from section-proof retract-proof) where

    to : _
    to Nothing  = inj₁ tt
    to (Just x) = inj₂ x

    from : _
    from (inj₁ tt) = Nothing
    from (inj₂ y)  = Just y

    section-proof : _
    section-proof (inj₁ tt) = refl
    section-proof (inj₂ _)  = refl

    retract-proof : _
    retract-proof Nothing  = refl
    retract-proof (Just _) = refl

  isSet-proof : ∀ {l} {A : Type l} → isSet A → isSet (Maybe A)
  isSet-proof {A = A} A/set = subst⁻ (λ x → isSet x) generic lemma where
    lemma : isSet (⊤ ⊎ A)
    lemma = isHLevel⊎ 0 (isContr→isOfHLevel 2 isContr⊤) A/set

  functor : ∀ {l} → Functor {lsuc l} {l} SET SET
  functor .Functor.mapₒ (a , a/set) = Maybe a , (isSet-proof a/set)
  functor .map→ f Nothing  = Nothing
  functor .map→ f (Just x) = Just (f x)
  functor {l} .Functor.map-id obj = funExt λ where
    Nothing  → refl
    (Just x) → refl
  functor {l} .Functor.map→-dist f g i Nothing  = Nothing
  functor {l} .Functor.map→-dist f g i (Just x) = Just (g (f x))
