{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Types.Fin where

open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism

open import Utils
open import Utils.Path
open import Utils.Void
open import Lattice

open import Types.Nat hiding (lte; gte)

import Types.Nat as Nat

record Fin (n : Nat) : Type where

  constructor Fin[_,_]

  field
    value : Nat
    finite : Nat.lte value n

lte : {n : Nat} → Fin n → Fin n → Type
lte Fin[ Z , _ ] Fin[ Z , _ ] = ⊤
lte Fin[ Z , _ ] Fin[ S y , _ ] = ⊤
lte Fin[ S x , _ ] Fin[ Z , _ ] = ⊥
lte {n} Fin[ S x , sx-fin ] Fin[ S y , sy-fin ] = Nat.lte x y

gte : {n : Nat} → Fin n → Fin n → Type
gte Fin[ Z , _ ] Fin[ Z , _ ] = ⊤
gte Fin[ Z , _ ] Fin[ S y , _ ] = ⊥
gte Fin[ S x , _ ] Fin[ Z , _ ] = ⊤
gte {n} Fin[ S x , sx-fin ] Fin[ S y , sy-fin ] = Nat.gte x y

open Fin
open PartialOrder

partial-order-lt : {n : Nat} → PartialOrder (Fin n)
partial-order-lt ._≤_ = lte

partial-order-lt {n} .isProp≤ = f  where
  f : (a b : Fin n) → isProp (lte a b)
  f Fin[ Z , _ ] Fin[ Z , _ ] = isProp⊤
  f Fin[ Z , _ ] Fin[ S b , _ ] = isProp⊤
  f Fin[ S a , _ ] Fin[ Z , _ ] = isProp⊥
  f Fin[ S a , _ ] Fin[ S b , _ ] = Nat.partial-order .isProp≤ a b

partial-order-lt {n} .reflexive Fin[ Z , _ ] = tt
partial-order-lt {n} .reflexive Fin[ S x , _ ] = Nat.partial-order .reflexive x

partial-order-lt {n} .trans = f where
  f : (a b c : Fin n) → lte a b → lte b c → lte a c
  f Fin[ Z , _ ] Fin[ Z , _ ] Fin[ Z , _ ] p q = tt
  f Fin[ Z , _ ] Fin[ Z , _ ] Fin[ S c , _ ] p q = tt
  f Fin[ Z , _ ] Fin[ S b , _ ] Fin[ S c , _ ] p q = tt
  f Fin[ S a , _ ] Fin[ S b , _ ] Fin[ S c , _ ] p q = Nat.partial-order .trans a b c p q

partial-order-lt {n} .anti-sym = f where
  f : (a b : Fin n) → lte a b → lte b a → a ≡ b
  f Fin[ Z , a-fin ] Fin[ Z , b-fin ] _ _ i =
    Fin[ Z , Nat.partial-order .isProp≤ Z Z a-fin b-fin i ]
  f Fin[ Z , _ ] Fin[ S b , _ ] _ q = absurd q
  f Fin[ S a , _ ] Fin[ Z , _ ] p _ = absurd p
  f Fin[ S a , sa-fin ] Fin[ S b , sb-fin ] p q i =
    Fin[ S (a≡b i) , fin≡ i ]
    where
      a≡b = Nat.partial-order .anti-sym a b p q

      fin≡ : PathP (λ i → Nat.lte (S (a≡b i)) n) sa-fin sb-fin
      fin≡ = isProp→PathP (λ j → Nat.partial-order .isProp≤ (S (a≡b j)) n) sa-fin sb-fin

partial-order-gt : {n : Nat} → PartialOrder (Fin n)
partial-order-gt ._≤_ = gte
partial-order-gt {n} .isProp≤ = f where
  f : (a b : Fin n) → isProp (gte a b)
  f Fin[ Z , _ ] Fin[ Z , _ ] = isProp⊤
  f Fin[ Z , _ ] Fin[ S b , _ ] = isProp⊥
  f Fin[ S a , _ ] Fin[ Z , _ ] = isProp⊤
  f Fin[ S a , _ ] Fin[ S b , _ ] = Nat.isProp-gte a b
partial-order-gt {n} .reflexive = f where
  f : (a : Fin n) → gte a a
  f Fin[ Z , _ ] = tt
  f Fin[ S a , _ ] = Nat.gte-reflexive a
partial-order-gt {n} .trans = f where
  f : (a b c : Fin n) → gte a b → gte b c → gte a c
  f Fin[ Z , _ ] Fin[ Z , _ ] Fin[ Z , _ ] p q = tt
  f Fin[ S a , _ ] Fin[ Z , _ ] Fin[ Z , _ ] p q = tt
  f Fin[ S a , _ ] Fin[ S b , _ ] Fin[ Z , _ ] p q = tt
  f Fin[ S a , _ ] Fin[ S b , _ ] Fin[ S c , _ ] p q = Nat.gte-trans a b c p q
partial-order-gt {n} .anti-sym = f  where
  f : (a b : Fin n) → gte a b → gte b a → a ≡ b
  f Fin[ Z , _ ] Fin[ Z , _ ] p q = refl
  f Fin[ S a , sa-fin ] Fin[ S b , sb-fin ] p q i =
    Fin[ S (a≡b i) , (fin≡ i) ]
    where
      a≡b = Nat.gte-anti-sym a b p q

      fin≡ : PathP (λ i → Nat.lte (S (a≡b i)) n) sa-fin sb-fin
      fin≡ = isProp→PathP (λ j → Nat.partial-order .isProp≤ (S (a≡b j)) n) sa-fin sb-fin
