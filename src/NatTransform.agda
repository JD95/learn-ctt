{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Category
open import Functor
open import Utils.Path
open import Utils.HLevels

open Category.Category
open Category.PreCategory
open Category.RawCategory
open Functor.Functor

module NatTransform where
  record
    NatTransform
      {lA lA' lB lB'}
      {C : Category lA lA'}
      {D : Category lB lB'}
      (F G : Functor C D)
    : Type (lsuc ((lA ⊔ lA') ⊔ (lB ⊔ lB')))
    where

    field

      η∘
        : (x : C .Object)
        → D .Arrow (F .map∘ x) (G .map∘ x)

      η→
        : {x y : C .Object}
        → (f : C .Arrow x y)
        → η∘ y ∙⟨ D ⟩ F .map→ f
        ≡ G .map→ f ∙⟨ D ⟩ η∘ x

  open NatTransform

  isSet-NatTransform
    : ∀ {lA lA' lB lB'}
    → {a : Category lA lA'}
    → {b : Category lB lB'}
    → {F G : Functor a b}
    → isSet (NatTransform F G)
  isSet-NatTransform {a = a} {b = b} {F = F} {G = G} x y ≡¹ ≡² i j = result where
    η∘≡ : (ap η∘ ≡¹) ≡ (ap η∘ ≡²)
    η∘≡ = (isSetΠ λ _ → b .isSet-Arrow) (x .η∘) (y .η∘) (ap η∘ ≡¹) (ap η∘ ≡²)

    η→≡ :
      SquareP (λ k l →
          {x y : a .Object}
          → (f : a .Arrow x y)
          → η∘≡ k l y ∙⟨ b ⟩ F .map→ f
          ≡ G .map→ f ∙⟨ b ⟩ η∘≡ k l x)
        (ap η→ ≡¹)
        (ap η→ ≡²)
        refl
        refl
    η→≡ = isSet→SquareP proof (ap η→ ≡¹) (ap η→ ≡²) refl refl where

      proof : (i : I) → (j : I) → _
      proof i j =
        isSetImplicitΠ2 λ _ _ → isSetΠ λ _ → isSet-Set≡ (b .isSet-Arrow)

    result : NatTransform F G
    result .η∘ = η∘≡ i j
    result .η→ = η→≡ i j

  IdNat
    : ∀ {lo la} {a b : Category lo la} {F : Functor a b}
    → NatTransform F F
  IdNat {b = b} .η∘ obj = b .id
  IdNat {b = b} {F = F} .η→ arr =
      b .id ∙⟨ b ⟩ F .map→ arr ≡⟨ b .seq-id-l _ ⟩
                   F .map→ arr ≡⟨ sym (b .seq-id-r _) ⟩
      F .map→ arr ∙⟨ b ⟩ b .id ∎

  _∙η⇓_
    : ∀ {lo la} {a b : Category lo la}
    → {F G H : Functor a b}
    → (N : NatTransform G H)
    → (M : NatTransform F G)
    → NatTransform F H
  _∙η⇓_ {b = b} M N .NatTransform.η∘ obj = M .η∘ obj ∙⟨ b ⟩ N .η∘ obj
  _∙η⇓_ {_} {_} {a} {b} {F} {G} {H} M N .NatTransform.η→ {x} {y} arr =
    let Mη = M .η→ arr
        Nη = N .η→ arr
    in (M .η∘ y ∙⟨ b ⟩ N .η∘ y) ∙⟨ b ⟩ F .map→ arr  ≡⟨ sym (b .seq-assoc _ _ _)  ⟩
               _ ∙⟨ b ⟩ (       _ ∙⟨ b ⟩ _          ) ≡⟨ (λ i → (M .η∘ y) ∙⟨ b ⟩ (N .η→ arr i)) ⟩
               _ ∙⟨ b ⟩ (G .map→ arr ∙⟨ b ⟩ N .η∘ x) ≡⟨ b .seq-assoc _ _ _ ⟩
       (M .η∘ y ∙⟨ b ⟩ G .map→ arr) ∙⟨ b ⟩ _         ≡⟨ (λ i → (M .η→ arr i) ∙⟨ b ⟩ N .η∘ x) ⟩
       (H .map→ arr ∙⟨ b ⟩ M .η∘ x) ∙⟨ b ⟩ _         ≡⟨ sym (b .seq-assoc _ _ _) ⟩
        H .map→ arr ∙⟨ b ⟩ (M .η∘ x ∙⟨ b ⟩ N .η∘ x) ∎
  _η⇓∙_
    : ∀ {lo la} {a b : Category lo la}
    → {F G H : Functor a b}
    → (M : NatTransform F G)
    → (N : NatTransform G H)
    → NatTransform F H
  _η⇓∙_ M N = N ∙η⇓ M

  _∙η⇒_
    : ∀ {lo la} {a b c : Category lo la}
    → {F₁ G₁ : Functor a b} {F₂ G₂ : Functor b c}
    → (N : NatTransform F₂ G₂)
    → (M : NatTransform F₁ G₁)
    → NatTransform (F₂ ∙F F₁) (G₂ ∙F G₁)
  _∙η⇒_ {c = c} N M .NatTransform.η∘ obj = N .η→ (M .η∘ obj) i1
  _∙η⇒_ {_} {_} {a} {b} {c} {F₁} {G₁} {F₂} {G₂} N M .NatTransform.η→ {x} {y} arr =
      (G₂ .map→ (M .η∘ y) ∙⟨ c ⟩ N .η∘ (F₁ .map∘ y)) ∙⟨ c ⟩ (F₂ .map→ (F₁ .map→ arr)) ≡⟨ sym (c .seq-assoc _ _ _) ⟩
                        _  ∙⟨ c ⟩ (                  _ ∙⟨ c ⟩ _                       ) ≡⟨ (λ i → G₂ .map→ (M .η∘ y) ∙⟨ c ⟩ (N .η→ (F₁ .map→ arr) i) ) ⟩
                        _  ∙⟨ c ⟩ (G₂ .map→ (F₁ .map→ arr) ∙⟨ c ⟩ N .η∘ (F₁ .map∘ x))  ≡⟨ c .seq-assoc _ _ _ ⟩
      (G₂ .map→ (M .η∘ y) ∙⟨ c ⟩ G₂ .map→ (F₁ .map→ arr)) ∙⟨ c ⟩ _                     ≡⟨ (λ i → (G₂ .map-dist (F₁ .map→ arr) (M .η∘ y) (~ i)) ∙⟨ c ⟩ N .η∘ (F₁ .map∘ x)) ⟩
       G₂ .map→  (M .η∘ y ∙⟨ b ⟩ F₁ .map→ arr)            ∙⟨ c ⟩ _                     ≡⟨ (λ i → G₂ .map→ (M .η→ arr i) ∙⟨ c ⟩ N .η∘ (F₁ .map∘ x)) ⟩
       G₂ .map→  (G₁ .map→ arr ∙⟨ b ⟩ M .η∘ x)            ∙⟨ c ⟩ _                     ≡⟨ (λ i → G₂ .map-dist (M .η∘ x) (G₁ .map→ arr) i ∙⟨ c ⟩ N .η∘ (F₁ .map∘ x)) ⟩
      (G₂ .map→ (G₁ .map→ arr) ∙⟨ c ⟩ (G₂ .map→ (M .η∘ x))) ∙⟨ c ⟩ _                   ≡⟨ sym (c .seq-assoc _ _ _) ⟩
       G₂ .map→ (G₁ .map→ arr) ∙⟨ c ⟩ (G₂ .map→ (M .η∘ x) ∙⟨ c ⟩ N .η∘ (F₁ .map∘ x))  ∎

  _η⇒∙_
    : ∀ {lo la} {a b c : Category lo la}
    → {F₁ G₁ : Functor a b} {F₂ G₂ : Functor b c}
    → (M : NatTransform F₁ G₁)
    → (N : NatTransform F₂ G₂)
    → NatTransform (F₂ ∙F F₁) (G₂ ∙F G₁)
  _η⇒∙_ M N = N ∙η⇒ M

  IsNatIso
    : ∀ {lo la} {a b : Category lo la}
    → {F G : Functor a b}
    → (N : NatTransform F G)
    → (M : NatTransform G F)
    → Type (lsuc lo ⊔ lsuc la)
  IsNatIso M N = M ∙η⇓ N ≡ IdNat

--  NatIsoFromObjIso
--    : ∀ {lo la} {a b : Category lo la}
--    → {F : Functor a b}
--    → (p : (x : b .Object) → Σ[ y ∈ b .Object ] x ≡ y)
--    → Σ[ G ∈ Functor a b ]
--      Σ[ N ∈ NatTransform F G ]
--      Σ[ M ∈ NatTransform G F ]
--      IsNatIso N M
--  NatIsoFromObjIso {a = a} {b = b} {F = F} p = G , N , M , NatIso
--    where
--     G = FunctorFromObjIso F p
--
--     N : NatTransform F G
--     N = {!!}
--
--     M : NatTransform G F
--     M = {!!}
--
--     NatIso : IsNatIso N M
--     NatIso = {!!}
