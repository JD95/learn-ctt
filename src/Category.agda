{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Utils

record RawCategory (lo la : Level) : Type (lsuc (lo ⊔ la)) where
  field
    Object : Type lo

    Arrow : Object → Object → Set la

    id : {a : Object} → Arrow a a

    seq
      : {a b c : Object}
      → Arrow b c
      → Arrow a b
      → Arrow a c

    -- laws
    seq-assoc
      : {a b c d : Object}
      → (f : Arrow a b)
      → (g : Arrow b c)
      → (h : Arrow c d)
      → seq h (seq g f) ≡ seq (seq h g) f

    seq-id-l
      : {a b : Object}
      → (f : Arrow a b)
      → seq (id {b}) f ≡ f

    seq-id-r
      : {a b : Object}
      → (f : Arrow a b)
      → seq f (id {a}) ≡ f

  InverseOf : {a b : Object} → Arrow a b → Arrow b a → Type la
  InverseOf {a} {b} f g = (seq f g ≡ id {b}) × (seq g f ≡ id {a})

  Isomorphism : {a b : Object} → Arrow a b → Type la
  Isomorphism {a} {b} f = Σ[ g ∈ Arrow b a ] (InverseOf f g)

  _≈_ : Object → Object → Type la
  _≈_ a b = Σ[ f ∈ Arrow a b ] (Isomorphism f)

  Epimorphism : {a b : Object} → (f : Arrow a b) → Type (lo ⊔ la)
  Epimorphism {a} {b} f =
    ∀ {x} → (g g' : Arrow b x) → seq g f ≡ seq g' f → g ≡ g'

  Monomorphism : {a b : Object} → (f : Arrow a b) → Type (lo ⊔ la)
  Monomorphism {a} {b} f =
    ∀ {x} → (g g' : Arrow x a) → seq f g ≡ seq f g' → g ≡ g'

  Initial : Object → Type (lo ⊔ la)
  Initial i = Σ[ x ∈ Object ] (isContr (Arrow i x))

  Terminal : Object → Type (lo ⊔ la)
  Terminal t = Σ[ x ∈ Object ] (isContr (Arrow x t))

record PreCategory (lo la : Level) : Type (lsuc (lo ⊔ la)) where
  field
    isRawCategory : RawCategory lo la
  open RawCategory isRawCategory public
  field
    isSet-Arrow
      : {x y : Object}
      → isSet (Arrow x y)

record Category (lo la : Level) : Type (lsuc (lo ⊔ la)) where
  field
    isPreCategory : PreCategory lo la

  open PreCategory isPreCategory public

  field
    ≈→≡ : {a b : Object} → a ≈ b → a ≡ b

  id-iso : (a : Object) → a ≈ a
  id-iso a = id , id , seq-id-l id , seq-id-l id

  ≡→≈ : {A B : Object} → A ≡ B → A ≈ B
  ≡→≈ {A} {B} eq = subst (λ X → A ≈ X) eq (id-iso A)

open Category

op : ∀ {lo la} (C : Category lo la) → Category lo la
op {lo} {la} C = op-cat where

  op-raw : _
  op-raw = record
   { Object = C .Object
   ; Arrow = λ x y → C .Arrow y x
   ; id = C .id
   ; seq = λ g f → C .seq f g
   ; seq-assoc = λ f g h → sym (C .seq-assoc h g f)
   ; seq-id-l = λ f → C .seq-id-r f
   ; seq-id-r = λ f → C .seq-id-l f
   }

  op-pre : _
  op-pre = record
    { isRawCategory = op-raw
    ; isSet-Arrow = C .isSet-Arrow
    }

  op-cat : Category lo la
  op-cat .Category.isPreCategory = op-pre
  op-cat .≈→≡ (arr , inv , proof) = C .≈→≡ (inv , arr , proof)

seqL
  : ∀ {lo la} (C : Category lo la)
  → {a b c : C .Object}
  → C. Arrow b c
  → C .Arrow a b
  → C .Arrow a c
seqL C g f =  C .seq g f

infixr 15 seqL
syntax seqL C g f = g ∙⟨ C ⟩ f

seqR
  : ∀ {lo la} (C : Category lo la)
  → {a b c : C .Object}
  → C. Arrow a b
  → C .Arrow b c
  → C .Arrow a c
seqR C f g =  C .seq g f

infixr 15 seqR
syntax seqR C f g = f ⟨ C ⟩∙ g
