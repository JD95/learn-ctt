{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Data.Nat using (suc; _+_)
open import Data.Sum.Base
open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Equiv
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

module Utils.Void where
  data ⊥ : Type where

  absurd : ∀ {l} {A : Type l} → ⊥ → A
  absurd ()

  isProp⊥ : isProp ⊥
  isProp⊥ () ()
