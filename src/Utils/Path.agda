{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Data.Nat using (suc; _+_)
open import Data.Sum.Base
open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Equiv
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

module Utils.Path where
  in₀ : ∀ {l} (p : I → Type l) → (i : I) → p i0 → p i
  in₀ {_} p i x = transp (λ j → p (i ∧ j)) (~ i) x

  in₀Dep
    : ∀ {l k} {A : Type l} {B : A → Type k}
    → (p : I → A)
    → B (p i0)
    → (i : I)
    → B (p i)
  in₀Dep {_} {_} {A} {B} p x i = transport proof x where
    proof : B (p i0) ≡ B (p i)
    proof j = B (p (i ∧ j))

  in₁ : ∀ {l} (p : I → Type l) → (i : I) → p i1 → p i
  in₁ {_} p i x = transp (λ j → p (i ∨ ~ j)) i x

  in₁Dep
    : ∀ {l k} {A : Type l} {B : A → Type k}
    → (p : I → A)
    → B (p i1)
    → (i : I)
    → B (p i)
  in₁Dep {_} {_} {A} {B} p x i = transport proof x where
    proof : B (p i1) ≡ B (p i)
    proof j = B (p (i ∨ ~ j))

  out₀ : ∀ {l} (p : I → Type l) → (i : I) → p i → p i0
  out₀ {_} p i x = transp (λ j → p (i ∧ ~ j)) (~ i) x

  out₀Dep
    : ∀ {l k} {A : Type l} {B : A → Type k}
    → (p : I → A)
    → (i : I)
    → B (p i)
    → B (p i0)
  out₀Dep {_} {_} {A} {B} p i x = transport⁻ proof x where
    proof : B (p i0) ≡ B (p i)
    proof j = B (p (i ∧ j))

  out₁ : ∀ {l} (p : I → Type l) → (i : I) → p i → p i1
  out₁ {_} p i x = transp (λ j → p (i ∨ j)) i x

  out₁Dep
    : ∀ {l k} {A : Type l} {B : A → Type k}
    → (p : I → A)
    → (i : I)
    → B (p i)
    → B (p i1)
  out₁Dep {_} {_} {A} {B} p i x = transport⁻ proof x where
    proof : B (p i1) ≡ B (p i)
    proof j = B (p (i ∨ ~ j))

  to≡ : ∀{l} {A : Type l} → (f : I → A) → f i0 ≡ f i1
  to≡ f i = f i

  toPath : ∀{l} {A : Type l} → {x y : A} (p : x ≡ y) → I → A
  toPath f i = f i

  ap : ∀{l m} {A : Type l} {B : A → Type m} (f : (a : A) → B a)
     → {x y : A} (p : x ≡ y)
     → PathP (λ i → B (p i)) (f x) (f y)
  ap f p i = f (p i)
