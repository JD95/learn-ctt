{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

module Utils.Properties where

open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism

record IsCommutative {l} {T : Type l} (op : T → T → T) : Type (lsuc l) where
  field
    comm : (a b : T) → op a b ≡ op b a

record IsAssociative {l} {T : Type l} (op : T → T → T) : Type (lsuc l) where
  field
    assoc : (a b c : T) → op (op a b) c ≡ op a (op b c)
