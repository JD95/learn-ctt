{-# OPTIONS --cubical #-}

open import Agda.Builtin.Unit
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.HLevels

module Utils.HLevels where

  isContr⊤ : isContr ⊤
  isContr⊤ = tt , (λ y i → tt)

  isSet→PathP
    : ∀ {l} (p : I → Type l)
    → (x : isSet (p i0))
    → (y : isSet (p i1))
    → PathP (λ i → isSet (p i)) x y
  isSet→PathP p x y = isProp→PathP (λ i → isPropIsSet {A = p i}) x y

  isSetImplicitΠ
    : ∀ {lA lB} {A : Type lA} {B : A → Type lB}
    → ((x : A) → isSet (B x))
    → isSet ({x : A} → B x)
  isSetImplicitΠ = λ h f g ≡¹ ≡² i j {x} →
    h x (f {x}) (g {x}) (λ k → ≡¹ k {x}) (λ k → ≡² k {x}) i j

  isSetImplicitΠ2
    : ∀ {lA lB lC} {A : Type lA} {B : Type lB} {C : A → B → Type lC}
    → ((x : A) → (y : B) → isSet (C x y))
    → isSet ({x : A} {y : B} → C x y)
  isSetImplicitΠ2 h = isSetImplicitΠ λ x → isSetImplicitΠ λ y → h x y

  isSet-Set≡
    : ∀ {l} {A : Type l}
    → isSet A
    → {a b : A}
    → isSet (a ≡ b)
  isSet-Set≡ a/set {a} {b} =
    isSet→isGroupoid a/set a b
