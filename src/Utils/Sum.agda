{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Data.Nat using (suc; _+_)
open import Data.Sum.Base
open import Agda.Primitive
open import Agda.Builtin.Unit
open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels

module Utils.Sum where

  open import Utils.Path
  open import Utils.Void

  inj₁-inj : ∀ {a b} {A : Type a} {B : Type b} {x y : A} → inj₁ {B = B} x ≡ inj₁ y → x ≡ y
  inj₁-inj {A = A} {B = B} {x = x} path = ap f path where
    f : A ⊎ B → A
    f (inj₁ x) = x
    f (inj₂ _) = x

  inj₂-inj : ∀ {a b} {A : Type a} {B : Type b} {x y : B} → inj₂ {A = A} x ≡ inj₂ y → x ≡ y
  inj₂-inj {A = A} {B = B} {x = x} path = ap f path where
    f : A ⊎ B → B
    f (inj₁ _) = x
    f (inj₂ x) = x

  ⊎-disjoint : ∀ {a b} {A : Type a} {B : Type b} {x : A} {y : B} → inj₁ x ≡ inj₂ y → ⊥
  ⊎-disjoint path = subst (λ { (inj₁ x) → ⊤ ; (inj₂ x) → ⊥ }) path tt

  Code : ∀ {la lb} {A : Type la} {B : Type lb} → A ⊎ B → A ⊎ B → Type (la ⊔ lb)
  Code {la} {lb} (inj₁ x) (inj₁ y) = Lift {la} {lb} (x ≡ y)
  Code           (inj₁ x) (inj₂ y) = Lift ⊥
  Code           (inj₂ x) (inj₁ y) = Lift ⊥
  Code {la} {lb} (inj₂ x) (inj₂ y) = Lift {lb} {la} (x ≡ y)

  decode : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B} → Code x y  → x ≡ y
  decode {x = inj₁ x} {y = inj₁ y} code = ap inj₁ (Lift.lower code)
  decode {x = inj₂ x} {y = inj₂ y} code = ap inj₂ (Lift.lower code)

  encode : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B} → x ≡ y → Code x y
  encode {x = inj₁ x} {y = inj₁ y} path = lift (inj₁-inj path)
  encode {x = inj₁ x} {y = inj₂ y} path = absurd (⊎-disjoint path)
  encode {x = inj₂ x} {y = inj₁ y} path = absurd (⊎-disjoint (sym path))
  encode {x = inj₂ x} {y = inj₂ y} path = lift (inj₂-inj path)

  decode-encode
    : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B}
    → (p : x ≡ y)
    → decode (encode p) ≡ p
  decode-encode {A = A} {B = B} = J (λ _ p → decode (encode p) ≡ p) d-e-refl where
    d-e-refl : {x : A ⊎ B} → decode (encode (λ i → x)) ≡ (λ i → x)
    d-e-refl {x = inj₁ x} = refl
    d-e-refl {x = inj₂ x} = refl

  encode-decode
    : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B}
    → (p : Code x y)
    → encode (decode p) ≡ p
  encode-decode {x = inj₁ x} {y = inj₁ y} p = refl
  encode-decode {x = inj₂ x} {y = inj₂ y} p = refl

  Code≃Path : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B} → Code x y ≃ (x ≡ y)
  Code≃Path {A = A} {B = B} {x} {y} = isoToEquiv (iso decode encode decode-encode encode-decode)

  isHLevelCode
    : ∀ {la lb} {A : Type la} {B : Type lb} {x y : A ⊎ B} {n : HLevel}
    → isOfHLevel (suc (suc n)) A
    → isOfHLevel (suc (suc n)) B
    → isOfHLevel (suc n) (Code x y)
  isHLevelCode {x = inj₁ x} {y = inj₁ y} {n} ahl bhl =
    isOfHLevelLift (suc n) (ahl x y)
  isHLevelCode {x = inj₂ x} {y = inj₂ y} {n} ahl bhl =
    isOfHLevelLift (suc n) (bhl x y)
  isHLevelCode {x = inj₁ x} {y = inj₂ y} {n} ahl bhl =
    isOfHLevelLift (suc n) (isProp→isOfHLevelSuc n λ x → absurd x)
  isHLevelCode {x = inj₂ x} {y = inj₁ y} {n} ahl bhl =
    isOfHLevelLift (suc n) (isProp→isOfHLevelSuc n λ x → absurd x)

  isHLevel⊎
    : ∀ {la lb} {A : Type la} {B : Type lb}
    → (n : HLevel)
    → isOfHLevel (2 + n) A
    → isOfHLevel (2 + n) B
    → isOfHLevel (2 + n) (A ⊎ B)
  isHLevel⊎ n ahl bhl x y = isOfHLevelRespectEquiv (1 + n) Code≃Path (isHLevelCode ahl bhl)
