{-# OPTIONS --cubical #-}
{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import Cubical.Core.Everything
open import Cubical.Foundations.Transport
open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.HLevels
open import Agda.Builtin.Sigma
open import Agda.Builtin.Cubical.Path

open import Utils
open import Category

open Category.Category

module Functor where
  record Functor
    {lC lC' lD lD'}
    (C : Category lC lC')
    (D : Category lD lD')
    : Type (lsuc ((lC ⊔ lC') ⊔ (lD ⊔ lD')))
    where

    field
      map∘ : C .Object → D .Object
      map→ : {a b : C .Object} → C .Arrow a b → D .Arrow (map∘ a) (map∘ b)
      map-id : (x : C .Object) → map→ (C .id {x}) ≡ D .id {map∘ x}
      map-dist
        : {a b c : C .Object}
        → (f : C .Arrow a b)
        → (g : C .Arrow b c)
        → map→ (g ∙⟨ C ⟩ f) ≡ map→ g ∙⟨ D ⟩ map→ f

  open Functor

  Contra
    : ∀ {lC lC' lD lD'}
    → (C : Category lC lC')
    → (D : Category lD lD')
    → Type (lsuc ((lC ⊔ lC') ⊔ (lD ⊔ lD')))
  Contra C D = Functor (op C) D

  IsFaithful
    : ∀ {lC lC' lD lD'} {C : Category lC lC'} {D : Category lD lD'}
    → (F : Functor C D)
    → Type _
  IsFaithful {C = C} F =
    {a b : C .Object} {f g : C .Arrow a b}
    → F .map→ f ≡ F .map→ g
    → f ≡ g

  IsFull
    : ∀ {lC lC' lD lD'} {C : Category lC lC'} {D : Category lD lD'}
    → (F : Functor C D)
    → Type _
  IsFull {C = C} {D = D} F =
    {a b : C .Object}
    → (g : D .Arrow (F .map∘ a) (F .map∘ b))
    → Σ[ f ∈  C .Arrow a b ] (F .map→ f ≡ g)

  _∙F_
    : ∀ {lA lA' lB lB' lC lC'} {A : Category lA lA'} {B : Category lB lB'} {C : Category lC lC'}
    → (G : Functor B C)
    → (F : Functor A B)
    → Functor A C
  _∙F_ G F .Functor.map∘ obj = G .map∘ (F .map∘ obj)
  _∙F_ G F .Functor.map→ arr = G .map→ (F .map→ arr)
  _∙F_ {A = A} {B = B} {C = C} G F .Functor.map-id obj =
    G .map→ (F .map→ (A .id)) ≡⟨ (λ i → G .map→ (F .map-id obj i)) ⟩
    G .map→          (B .id)  ≡⟨ G .map-id _ ⟩
                      C .id   ∎
  _∙F_ {A = A} {B = B} {C = C} G F .Functor.map-dist f g =
     G .map→  (F .map→ (g   ∙⟨ A ⟩                   f)) ≡⟨ (λ i → G .map→ (F .map-dist f g i)) ⟩
     G .map→ ((F .map→  g)  ∙⟨ B ⟩          (F .map→ f)) ≡⟨ G .map-dist _ _ ⟩
    (G .map→  (F .map→  g)) ∙⟨ C ⟩ (G .map→ (F .map→ f)) ∎

  _F∙_
    : ∀ {lA lA' lB lB' lC lC'} {A : Category lA lA'} {B : Category lB lB'} {C : Category lC lC'}
    → (F : Functor A B)
    → (G : Functor B C)
    → Functor A C
  F F∙ G = G ∙F F

  FunctorFromObjIso
    : ∀ {lA lA' lB lB'} {a : Category lA lA'} {b : Category lB lB'}
    → (F : Functor a b)
    → (p : (x : b .Object) → Σ[ y ∈ b .Object ] x ≡ y)
    → Functor a b
  FunctorFromObjIso {a = a} {b = b} F p = G where
     G : Functor a b
     G .Functor.map∘ obj = p (F .map∘ obj) .fst
     G .Functor.map→ {x} {y} arr =
       let (_ , idA , _) = ≡→≈ b (p (F .map∘ x) .snd)
           (idB , _ , _) = ≡→≈ b (p (F .map∘ y) .snd)
       in idB ∙⟨ b ⟩ F .map→ arr ∙⟨ b ⟩ idA
     G .Functor.map-id obj =
       let (idB , idA , inv) = ≡→≈ b (p (F .map∘ obj) .snd)
       in

       idB ∙⟨ b ⟩ (F .map→ (a .id) ∙⟨ b ⟩ idA) ≡⟨ (λ i → idB ∙⟨ b ⟩ (F .map-id _ i) ∙⟨ b ⟩ idA) ⟩
       idB ∙⟨ b ⟩ (b .id  ∙⟨ b ⟩ idA)          ≡⟨ (b .seq-assoc _ _ _) ⟩
       (idB ∙⟨ b ⟩  b .id) ∙⟨ b ⟩ idA          ≡⟨ (λ i → (b .seq-id-r idB i) ∙⟨ b ⟩ idA ) ⟩
       idB ∙⟨ b ⟩ idA                          ≡⟨ fst inv ⟩
       b .id                                   ∎

     G .Functor.map-dist {x} {y} {z} f g =
       let (idX , id⁻X , x-sec , x-ret) = ≡→≈ b (p (F .map∘ x) .snd)
           (idY , id⁻Y , y-sec , y-ret) = ≡→≈ b (p (F .map∘ y) .snd)
           (idZ , id⁻Z , z-sec , z-ret) = ≡→≈ b (p (F .map∘ z) .snd)
           end-bit = ((F .map→ f) ∙⟨ b ⟩ id⁻X)

       in
       idZ  ∙⟨ b ⟩ (F .map→ (g ∙⟨ a ⟩ f)         ∙⟨ b ⟩ id⁻X)           ≡⟨ (λ i → idZ ∙⟨ b ⟩ (F .map-dist f g i) ∙⟨ b ⟩ id⁻X) ⟩
       _    ∙⟨ b ⟩ ((F .map→ g ∙⟨ b ⟩ F .map→ f) ∙⟨ b ⟩ _)              ≡⟨ (λ i → idZ ∙⟨ b ⟩ (b .seq-assoc id⁻X (F .map→ f) (F .map→ g) (~ i))) ⟩
       _    ∙⟨ b ⟩ (F .map→ g ∙⟨ b ⟩ end-bit)                           ≡⟨ (λ i → idZ ∙⟨ b ⟩ (F .map→ g) ∙⟨ b ⟩ (b .seq-id-l end-bit (~ i))) ⟩
       _    ∙⟨ b ⟩ (F .map→ g ∙⟨ b ⟩ (b .id ∙⟨ b ⟩ _))                  ≡⟨ (λ i → idZ ∙⟨ b ⟩ (F .map→ g) ∙⟨ b ⟩ (y-ret (~ i) ∙⟨ b ⟩ end-bit)) ⟩
       _    ∙⟨ b ⟩ (F .map→ g ∙⟨ b ⟩ ((id⁻Y ∙⟨ b ⟩ idY) ∙⟨ b ⟩ _))      ≡⟨ (λ i → idZ ∙⟨ b ⟩ (F .map→ g) ∙⟨ b ⟩ (sym (b .seq-assoc end-bit idY id⁻Y) i)) ⟩
       _    ∙⟨ b ⟩ (F .map→ g ∙⟨ b ⟩ (_     ∙⟨ b ⟩ _))                  ≡⟨ (λ i → idZ ∙⟨ b ⟩ (b .seq-assoc (idY ∙⟨ b ⟩ end-bit) id⁻Y (F .map→ g) i)) ⟩
       _    ∙⟨ b ⟩ ((F .map→ g ∙⟨ b ⟩ id⁻Y) ∙⟨ b ⟩ _)                   ≡⟨ b .seq-assoc _ _ _ ⟩
       (idZ ∙⟨ b ⟩ (F .map→ g ∙⟨ b ⟩ id⁻Y)) ∙⟨ b ⟩ (idY ∙⟨ b ⟩ end-bit) ∎
